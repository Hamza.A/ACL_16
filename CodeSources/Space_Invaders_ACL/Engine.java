
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JFrame;

public class Engine extends Thread {

	Graph graph;

	ArrayList<Tir> tab_tirs = new ArrayList<Tir>();
	ArrayList<Aliens> tab_aliens = new ArrayList<Aliens>();

	Vaisseau v = new Vaisseau();

	Engine() {

		// ajout d'aliens dans tab_aliens � la position point(x,y)
		tab_aliens.add(new Aliens(new Point(50, 50)));
		tab_aliens.add(new Aliens(new Point(100, 150)));
		tab_aliens.add(new Aliens(new Point(150, 100)));
		tab_aliens.add(new Aliens(new Point(200, 200)));
		tab_aliens.add(new Aliens(new Point(250, 250)));

		graph = new Graph();
		Events E = new Events(this);
		JFrame frame = new JFrame();

		// titre de la fenetre
		frame.setTitle("Space Invaders");
		// dimensions de la fen�tre
		frame.setSize(800, 600);
		// positionne la fen�tre au milieu de l'�cran
		frame.setLocationRelativeTo(null);
		// ferme la fen�tre si on appui sur la croix
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// on ajoute addKey(E) � notre fen�tre
		frame.addKeyListener(E);
		frame.setContentPane(graph);
		frame.setVisible(true);

		this.start();
		refresh();

	}

	// notre processus/boucle infinie
	public void run() {
		while (true) {

			// On parcourt le tableau des diff�rents tirs
			for (int i = 0; i < tab_tirs.size(); i++) {

				// appel le timer tic de la classe Tir (qui renvoit true si le
				// tir peut disaparaitre)
				if (tab_tirs.get(i).tic()) {
					tab_tirs.remove(i);
					i--;
				}

			}

			refresh();

			// timer
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}

	}

	// Consruit un tir � partir d'un point (de coordonn�e x,y)
	public void creer_tirs(Point point) {

		//ajoute un nouveau tir de coordonn�e point(x,y) dans tab_tirs
		tab_tirs.add(new Tir(point.x, point.y));

		//verifie si il y a collision entre le tir et les aliens
		for (int i = 0; i < tab_aliens.size(); i++) {
			tab_aliens.get(i).collision(v.pos_vaisseau_x, v.pos_vaisseau_x + 100, v.pos_vaisseau_y);

			//si il y a collision, et si l'alien n'a plus de vie, supprime l'alien i de tab_aliens
			if (tab_aliens.get(i).tic()) {
				tab_aliens.remove(i);
				i--;
			}
		}

	}

	// Consruit un alien � partir d'un point (de coordonn�e x,y)
	public void creer_aliens(Point point) {
		tab_aliens.add(new Aliens(point));
	}

	// supprime l'�lement i du tableau tab_aliens
	public void sup_aliens(int i) {
		tab_aliens.remove(i);
	}

	public void refresh() {

		// supprime les elements affich�s sur le graph
		graph.clearEntite();

		// dessine le vaisseau
		graph.paint_vaisseau(v);

		// dessine les tirs
		for (int i = 0; i < tab_tirs.size(); i++) {
			graph.afficher_tirs(tab_tirs.get(i).get_Coord());
		}

		// dessine les aliens
		for (int i = 0; i < tab_aliens.size(); i++) {
			graph.afficher_aliens(tab_aliens.get(i).get_Coord());
		}

		graph.repaint();
	}

	/**
	 * @param keyCode
	 */
	public void key(int keyCode) {
		// TODO Auto-generated method stub
		// keyCode : gauche : 37; haut : 38; droite : 39; bas : 40; a : 65;

		System.out.println(keyCode);

		// fleche gauche
		if (keyCode == 37 && 0 < v.pos_vaisseau_x){

			v.pos_vaisseau_x -= 10;
			for (int i = 0; i < tab_aliens.size(); i++) {
				if (tab_aliens.get(i).get_Coord().getX()==v.pos_vaisseau_x && tab_aliens.get(i).get_Coord().getY()==v.pos_vaisseau_y){
					sup_aliens(i);
				}}}

		// fleche haut
		if (keyCode == 38 && 0 < v.pos_vaisseau_y){
			v.pos_vaisseau_y -= 10;
			for (int i = 0; i < tab_aliens.size(); i++) {
				if (tab_aliens.get(i).get_Coord().getX()==v.pos_vaisseau_x && tab_aliens.get(i).get_Coord().getY()==v.pos_vaisseau_y){
					sup_aliens(i);
				}}}
		// fleche droite
		if (keyCode == 39 && graph.getWidth() > v.pos_vaisseau_x + 25){
			v.pos_vaisseau_x += 10;
			for (int i = 0; i < tab_aliens.size(); i++) {
				if (tab_aliens.get(i).get_Coord().getX()==v.pos_vaisseau_x && tab_aliens.get(i).get_Coord().getY()==v.pos_vaisseau_y){
					sup_aliens(i);
				}}}
		// fleche bas
		if (keyCode == 40 && graph.getHeight() > v.pos_vaisseau_y + 24){
			v.pos_vaisseau_y += 10 ;
			for (int i = 0; i < tab_aliens.size(); i++) {
				if (tab_aliens.get(i).get_Coord().getX()==v.pos_vaisseau_x && tab_aliens.get(i).get_Coord().getY()==v.pos_vaisseau_y){
					sup_aliens(i);
				}
			}}

		// touche a : tir
		if (keyCode == 65) {
			//creer_tirs(new Point(v.pos_vaisseau_x, v.pos_vaisseau_y));
			//Missiles m=new Missiles(v.pos_vaisseau_x,v.pos_vaisseau_y);
			//graph.paint_missile(m);
			for (int i = 0; i < tab_aliens.size(); i++) {
				if (tab_aliens.get(i).get_Coord().getX()==v.pos_vaisseau_x){
					sup_aliens(i);
				}
			}
		}

	}

}
